//
//  Constant.swift
//  mydata
//
//  Created by Nguyen Truong Dai Vi on 7/13/17.
//  Copyright © 2017 Nguyen Truong Dai Vi. All rights reserved.
//

import Foundation
struct Type {
    static let image = "IMAGE"
    static let video = "VIDEO"
    static let contact = "CONTACT"
    static let calendar = "CALENDAR"
    static let CSV = "CSV"
}
