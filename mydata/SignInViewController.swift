//
//  BackUpViewController.swift
//  mydata
//
//  Created by Nguyen Truong Dai Vi on 7/5/17.
//  Copyright © 2017 Nguyen Truong Dai Vi. All rights reserved.
//

import UIKit
//import GoogleAPIClientForREST
import GoogleSignIn



class SignInViewController: UIViewController, GIDSignInDelegate, GIDSignInUIDelegate {

    @IBOutlet weak var buttonView: UIView!
    var googleServiceManager = GoogleServiceManager.shareInstance
    
    // check feature is backup or restore
    var checkFeature: String!
    
    override func viewDidLoad() {
        
        googleServiceManager.signInSilently()
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        googleServiceManager.signInSilently()
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error) != nil {
            googleServiceManager.service.authorizer = nil
        } else {
            googleServiceManager.service.authorizer = user.authentication.fetcherAuthorizer()
            navigationController?.popViewController(animated: false)
        }
    }
}

//methods util

extension SignInViewController {
    func showAlert(title: String, message: String){
        let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alertVC.addAction(alertAction)
        present(alertVC, animated: true, completion: nil)
    }
}
