//
//  DataHelper.swift
//  mydata
//
//  Created by Nguyen Truong Dai Vi on 7/20/17.
//  Copyright © 2017 Nguyen Truong Dai Vi. All rights reserved.
//

import Foundation
import UIKit
import Contacts
import EventKit
import DKImagePickerController
import Photos

class DataHelper {
    var eventStore: EKEventStore
    
    static var shareInstance = DataHelper()
    init() {
        eventStore = EKEventStore()
    }
    
}

// methods import data
extension DataHelper {
    
    //import data to contacts
    func importToContacts(url: URL){
        // get strings from file csv
        var rawContacts = [String]()
        do {
            let text: String = try String(contentsOf: url)
            rawContacts = text.components(separatedBy: "\n")
        }catch let err {
            print(err)
        }
        // pass data to newContacts's field
        for string in rawContacts {
            if string != "" {
                let contactDetails = string.components(separatedBy: ",")
                let newContact = CNMutableContact()
                //given name, name prefix, name suffix, middle name,organization name, department name, birthday, number phone
                newContact.givenName = contactDetails[0]
                newContact.namePrefix = contactDetails[1]
                newContact.nameSuffix = contactDetails[2]
                newContact.middleName = contactDetails[3]
                newContact.organizationName = contactDetails[4]
                newContact.departmentName = contactDetails[5]
                if contactDetails[6] != "null"{
                    let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
                    let date = formatter.date(from: contactDetails[6])
                    // Returns "Jul 27, 2015, 12:29 PM" PST
                    var birthday = DateComponents()
                    birthday.year = Calendar.current.component(.year, from: date!)
                    birthday.month = Calendar.current.component(.month, from: date!)
                    birthday.day = Calendar.current.component(.day, from: date!)
                    newContact.birthday = birthday
                    
                }
                
                if contactDetails[7] != "null" {
                    let phoneNumber = CNLabeledValue(label: CNLabelHome, value: CNPhoneNumber(stringValue: contactDetails[7]))
                    newContact.phoneNumbers = [phoneNumber]
                }
                
                //send request to create a new contact
                do {
                    let saveRequest = CNSaveRequest()
                    saveRequest.add(newContact, toContainerWithIdentifier: nil)
                    let contactStore = CNContactStore()
                    try contactStore.execute(saveRequest)
                }catch let err {
                    print(err.localizedDescription)
                }
            }
        }
        
    }
    
    //get permission to access calendar
    func SOGetPermissionCalendarAccess() {
        
        switch EKEventStore.authorizationStatus(for: .event) {
            
        case .authorized:
            
            print("Authorised")
            
        case .denied:
            
            print("Access denied")
            
        case .notDetermined:
            
            // 3
            
            eventStore.requestAccess(to: .event, completion: {
                granted, error in
                if granted {
                    print("access success!")
                }else {
                    print(error.debugDescription)
                }
            })
        default:
            
            print("Case Default")
            
        }
        
    }
    
    //method import events to calendar
    func importToCalendar(url: URL, calendarName: String) throws{
        
        // get strings from file csv
        var rawEvents = [String]()
        do {
            let text: String = try String(contentsOf: url)
            rawEvents = text.components(separatedBy: "\n")
        }catch let err {
            print(err)
        }
        
        // 'EKEntityTypeReminder' or 'EKEntityTypeEvent'
        
        self.SOGetPermissionCalendarAccess()
        
        // Use Event Store to create a new calendar instance
        // Configure its title
        let newCalendar = EKCalendar(for: .event, eventStore: self.eventStore)
        
        // Probably want to prevent someone from saving a calendar
        // if they don't type in a name...
        newCalendar.title = calendarName
        
        // Filter the available sources and select the "Local" source to assign to the new calendar's
        // source property
        var localSource: EKSource?
        for source in eventStore.sources {
            if source.sourceType == EKSourceType.calDAV && source.title == "iCloud" {
                localSource = source
                break
            }
            if source.sourceType == EKSourceType.local {
                localSource = source
                break
            }
        }
        if localSource != nil {
            newCalendar.source = localSource!
        }
        
        // Save the calendar using the Event Store instance
        try eventStore.saveCalendar(newCalendar, commit: true)
            UserDefaults.standard.set(newCalendar.calendarIdentifier, forKey: "EventTrackerPrimaryCalendar")
            
        // add events to new calendar
        for eventString in rawEvents {
            if eventString != "" {
                let event:EKEvent = EKEvent(eventStore: eventStore)
                var events = eventString.components(separatedBy: ",")
                event.title = events[0]
                
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
                // Returns "Jul 27, 2015, 12:29 PM" PST
                event.startDate = formatter.date(from: events[1])!
                
                var endDate = events[2]
                let range = endDate.index(endDate.endIndex, offsetBy: -1)..<endDate.endIndex
                endDate.removeSubrange(range)
                event.endDate = formatter.date(from: endDate)!
                event.notes = "this is note."
                event.calendar = newCalendar
                try eventStore.save(event, span: .thisEvent)
            }
        }
    }
    
    //write image,video to library
    func writeImage_VideoToLibrary(data: Data, fileDescription: String){
        
        let date = Date()
        self.createDirectory(name: "MyDataApp")
        // write data to library camera roll
        PHPhotoLibrary.shared().performChanges({
            if fileDescription.contains("video") {
                // write video
                let url = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent("MyDataApp/video_\(date).mov")
                try? data.write(to: url)
                PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: url)
            }else {
                //write image
                let url = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent("MyDataApp/img_\(date).jpeg")
                try? data.write(to: url)
                PHAssetChangeRequest.creationRequestForAssetFromImage(atFileURL: url)
            }
            
        }) { saved, err in
            if err != nil {
                print(err.debugDescription)
            }
        }
    }
    
}


//methods get data, export data
extension DataHelper {
    // get data video from AVAsset
    func getDataAV(asset: DKAsset, name: String, complete: @escaping (NSData?)->()){
        var path = (NSTemporaryDirectory() as NSString).appendingPathComponent("temp\(name)")
        try? FileManager.default.removeItem(at: URL(fileURLWithPath: path))
        path = (NSTemporaryDirectory() as NSString).appendingPathComponent("temp\(name)")
        asset.writeAVToFile(path, presetName: AVAssetExportPresetMediumQuality, completeBlock: { (success) in
            var data: NSData? = nil
            if success {
                data = NSData(contentsOf: URL(fileURLWithPath: path))
                try? FileManager.default.removeItem(at: URL(fileURLWithPath: path))
            } else {
                print("transfer error!")
            }
            complete(data)
        })
    }
    
    
    //export contacts to file csv
    func exportContactCSV(contacts: [CNContact]) -> URL {
        let date = Date()
        let fileName = "\(date)_contacts.csv"
        let url = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent(fileName)
//            URL(fileURLWithPath: NSTemporaryDirectory().appending(fileName))
        var csvText = ""
        var newLine = ""
        for contact in contacts {
            //given name, name prefix, name suffix, middle name, organization name, department name, birthday, number phone
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
            var birthday: String? = nil
            if contact.birthday != nil {
                birthday = formatter.string(from: (contact.birthday?.date)!)
            }
            
            newLine = "\(contact.givenName),\(contact.namePrefix),\(contact.nameSuffix),\(contact.middleName),\(contact.organizationName),\(contact.departmentName),\(birthday != nil ? birthday! : "null"),\((contact.phoneNumbers.first != nil ? contact.phoneNumbers.first!.value.stringValue : "null"))"
        
            csvText.append(newLine)
            newLine = ""
        }
        do {
            try csvText.write(to: url, atomically: true, encoding: String.Encoding.utf8)
        }catch {
            print("error to create file")
        }
        return url
    }
    
    //write event calendar to file text (txt)
    func writeEventCalendarToTextFile(events: [EKEvent], name: String) -> URL? {
        let file = "\(Date())_\(name)_calendar.txt" //this is the file. we will write to and read from it
        
        var text = "" //just a text
        for event in events {
            text += "\(event.title),\(event.startDate),\(event.endDate)\n"
        }
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            let path = dir.appendingPathComponent(file)
            
            //writing
            do {
                try text.write(to: path, atomically: false, encoding: String.Encoding.utf8)
            }
            catch let err as NSError {
                print(err.debugDescription)
            }
            return path
        }
        return nil
    }
}

extension DataHelper{
    func createDirectory(name: String) {
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let dataPath = documentsDirectory.appendingPathComponent(name)
        
        do {
            try FileManager.default.createDirectory(atPath: dataPath.path, withIntermediateDirectories: true, attributes: nil)
        } catch let error as NSError {
            print("Error creating directory: \(error.localizedDescription)")
        }
    }
    
    func moveFile(from current: URL,to desUrl: URL){
        let des = desUrl.appendingPathComponent(current.lastPathComponent)
        do{
            try FileManager.default.moveItem(at: current, to: des)
        }catch let err as NSError{
            print(err.debugDescription)
        }
    }
    
    func deleteInPHAsset(imageUrls: [URL]){
        PHPhotoLibrary.shared().performChanges({
            let imageAssetToDelete = PHAsset.fetchAssets(withALAssetURLs: imageUrls, options: nil)
            PHAssetChangeRequest.deleteAssets(imageAssetToDelete)
        }, completionHandler: {success, error in
            print(success ? "Success" : error.debugDescription )
        })
    }
}
