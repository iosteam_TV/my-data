//
//  ViewController.swift
//  mydata
//
//  Created by Nguyen Truong Dai Vi on 7/4/17.
//  Copyright © 2017 Nguyen Truong Dai Vi. All rights reserved.
//

import UIKit

class GoogleServiceController: UIViewController {

    var googleService = GoogleServiceManager.shareInstance
    var checkFeatur = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func viewDidAppear(_ animated: Bool) {
        if checkFeatur == "backup" {
            if googleService.isSignIn() {
                checkFeatur = ""
                performSegue(withIdentifier: "backup", sender: nil)
            }
        }else if checkFeatur == "restore"{
            if googleService.isSignIn() {
                checkFeatur = ""
                performSegue(withIdentifier: "restore", sender: nil)
            }
        }
        
    }

    @IBAction func backupBtnPressed(_ sender: Any) {
        if googleService.isSignIn() {
            performSegue(withIdentifier: "backup", sender: nil)
        } else {
            checkFeatur = "backup"
            performSegue(withIdentifier: "signin", sender: "backup")
        }
    }
    
    @IBAction func restoreBtnPressed(_ sender: Any) {
        if googleService.isSignIn() {
            performSegue(withIdentifier: "restore", sender: nil)
        } else {
            checkFeatur = "restore"
            performSegue(withIdentifier: "signin", sender: "restore")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "signin" {
            if let signInVC = segue.destination as? SignInViewController {
                signInVC.checkFeature = sender as? String
            }
        }
    }
}

