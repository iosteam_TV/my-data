//
//  FileCell.swift
//  mydata
//
//  Created by Nguyen Truong Dai Vi on 8/10/17.
//  Copyright © 2017 Nguyen Truong Dai Vi. All rights reserved.
//

import UIKit

class FileCell: UITableViewCell {

    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var fileNameLbl: UILabel!
    
    func configUI(name: String) {
        if !name.contains(".") {
            icon.image = UIImage(named: "icons8-folder")
        }else {
            icon.image = UIImage(named: "icons8-file")
        }
        fileNameLbl.text = name
    }
}
