//
//  SelectDataTransferViewController.swift
//  mydata
//
//  Created by Nguyen Truong Dai Vi on 7/9/17.
//  Copyright © 2017 Nguyen Truong Dai Vi. All rights reserved.
//

import UIKit
import Photos
import MobileCoreServices
import DKImagePickerController // handle choose multiple image, video
import Contacts
import ContactsUI
import EventKit
import EventKitUI

class SelectDataTransferViewController: UIViewController {
    
    // declare pickerController to handle image, video picker
    var pickerController = DKImagePickerController()
    var listVideo: [NSData?] = []
    var listImage: [NSData?] = []
    var urlContact: URL?
    let transferManager = TransferServiceManager.shareInstance
    
    // declare to prepare handle calendar event
    var events: [EKEvent] = []
    var eventStore = EKEventStore()
    var calendars: [EKCalendar]?
    var calendarChoosed: [String]?
    var totalData = 0
    var saveAmountImgVideo = 0
    var countSend = 0
    
    @IBOutlet weak var transferBtn: UIButton!
    @IBOutlet weak var calendarTableView: UITableView!
    @IBOutlet weak var calendarView: UIView!
    @IBOutlet weak var transferProgress: UIProgressView!
    
    @IBOutlet weak var countLbl: UILabel!
    @IBOutlet weak var photoSwitch: UISwitch!
    @IBOutlet weak var contactSwitch: UISwitch!
    @IBOutlet weak var calendarSwitch: UISwitch!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //set up calendar view
        calendarView.isHidden = true
        calendarView.layer.cornerRadius = 10
        calendarView.layer.shadowColor = UIColor.black.cgColor
        calendarView.layer.shadowOffset = CGSize.zero
        calendarView.layer.shadowOpacity = 1
        DataHelper.shareInstance.SOGetPermissionCalendarAccess()
        
        //set up transferManager
        self.transferManager.delegate = self
        
        //initial calendarChoose
        calendarChoosed = []
        
        //setup image picker
        self.pickerController.showsCancelButton = true
        
        //setup progress
        if self.transferManager.totalData > 0 {
            let percent = Float(self.transferManager.countProgress) / Float(self.transferManager.totalData)
            transferProgress.setProgress(percent, animated: true)
            self.countLbl.text = "\(self.transferManager.countProgress)/\(self.transferManager.totalData)"
        }else {
            self.resetProgress()
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.transferManager.send(code: "back")
    }
    
    //handel calendar switched
    @IBAction func calendarSwiched(_ sender: UISwitch) {
        if sender.isOn {
            self.calendars = eventStore.calendars(for: .event)
            self.calendarTableView.delegate = self
            self.calendarTableView.dataSource = self
            self.calendarView.isHidden = false
            self.view.layer.backgroundColor = UIColor.darkGray.cgColor
            self.transferBtn.isHidden = true
            self.calendarTableView.reloadData()
        }else {
            if totalData > 0 && (events.count) > 0{
               totalData -= 1
            }
            calendarChoosed = []
            events = []
        }
    }
    
    //handel contact switched
    @IBAction func contactSwitched(_ sender: UISwitch) {
        if sender.isOn {
            let cnPicker = CNContactPickerViewController()
            cnPicker.delegate = self
            self.present(cnPicker, animated: true, completion: nil)
        } else {
            if totalData > 0 && urlContact != nil {
                self.totalData -= 1
            }
            urlContact = nil
        }
    }
    
    //handel photo, video switched
    @IBAction func backupPhotoSwitched(_ sender: UISwitch) {
        if sender.isOn {
            self.present(pickerController, animated: true) {}
            pickerController.didSelectAssets = { (assets: [DKAsset]) in
                self.totalData += assets.count
                self.saveAmountImgVideo = assets.count
                for i in 0..<assets.count {
                    if assets[i].isVideo {
                        DataHelper.shareInstance.getDataAV(asset: assets[i], name: "\(i)", complete: {
                            data in
                            self.listVideo.append(data!)
                        })
                    } else {
                        assets[i].fetchImageDataForAsset(true, completeBlock: {
                            data, info in
                            self.listImage.append(data! as NSData)
                        })
                    }
                }
            }
            
            print("didSelectAssets")
        } else {
            if totalData > 0 {
                self.totalData -= saveAmountImgVideo
                saveAmountImgVideo = 0
            }
            listVideo = []
            listImage = []
        }
    }
    
    @IBAction func cancelBtnPressed(_ sender: Any) {
        calendarView.isHidden = true
        self.view.layer.backgroundColor = UIColor.white.cgColor
        self.transferBtn.isHidden = false
    }
    
    //handel backup pressed
    @IBAction func transferBtnPressed(_ sender: Any) {
        if listImage.count > 0 || listVideo.count > 0 || urlContact != nil || (events.count) > 0 {
            self.present(UIHelper.showAlert(title: "Transfering", message: "Please Wait..."), animated: true, completion: nil)
//            let alert = UIAlertController(title: "Transfering", message: "Please Wait...", preferredStyle: .alert)
//            self.present(alert, animated: true, completion: nil)
        }
        
        //send total data
        self.transferManager.send(code: "total:\(self.totalData)")
        self.transferManager.totalData = self.totalData
        
        //send image
        if listImage.count > 0 {
            var i = 0
            for data in self.listImage {
                DispatchQueue.main.async {
                    self.transferManager.sendFile(data: data! as Data, name: "\(Type.image)\(i)")
                    i += 1
                }
            }
            listImage = []
        }
        
        // send video
        if listVideo.count > 0 {
            var i = 0
            for data in listVideo {
                DispatchQueue.main.async {
                    self.transferManager.sendFile(data: data! as Data, name: "\(Type.video)\(i)")
                    i += 1
                }
            }
            listVideo = []
        }
        
        // send contacts
        if self.urlContact != nil {
            var i = 0
            let data = try? Data(contentsOf: urlContact!)
            DispatchQueue.main.async {
                self.transferManager.sendFile(data: data! as Data, name: "\(Type.contact)\(i)")
            }
            i += 1
            self.urlContact = nil
        }
        
        //send calendar
        if (self.calendarChoosed?.count)! > 0 {
            var i = 0
            if (self.events.count) > 0 {
                let url = DataHelper.shareInstance.writeEventCalendarToTextFile(events: self.events, name: "Transfer")
                let data = NSData(contentsOf: url!)
                DispatchQueue.main.async {
                    self.transferManager.sendFile(data: data! as Data, name: "\(Type.calendar)\(i)")
                }
                i += 1
            }
            calendarChoosed = []
        }
        //reset all
        pickerController.deselectAllAssets()
        self.totalData = 0
        self.transferManager.totalData = 0
        self.photoSwitch.isOn = false
        self.contactSwitch.isOn = false
        self.calendarSwitch.isOn = false
    }
}

//implement methods of TransferServiceManagerDelegate
extension SelectDataTransferViewController: TransferServiceManagerDelegate {
    
    func connectedDevicesChanged(manager: TransferServiceManager, connectedDevices: [String]) {
        DispatchQueue.main.async {
            //dismiss all alert previous
            self.dismiss(animated: false, completion: nil)
            if connectedDevices.count == 0 {
                let alert = UIAlertController(title: "Errors", message: "No device connected!", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: {
                    alertController in
                    self.navigationController?.popViewController(animated: false)
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func didReceivedCode(manager: TransferServiceManager, code: String) {
        if code.contains("total") {
            let values = code.components(separatedBy: ":")
            self.totalData = Int(values[1])!
            self.transferManager.totalData = self.totalData
        }
        if code == "sendSuccess" {
            self.dismiss(animated: false, completion: nil)
            self.present(UIHelper.showAlert(title: "Send Success!", message: "Success!"), animated: true, completion: nil)
        } else if code == "fail" {
            self.dismiss(animated: false, completion: nil)
            self.present(UIHelper.showAlert(title: "Fail", message: "Have some errors!"), animated: true, completion: nil)
        }
        else if code == "back" {
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    func didReceivedData(manager: TransferServiceManager, withURL: URL?, error: Error?) {
        
        DispatchQueue.main.async {
            self.transferManager.countProgress += 1
            self.countLbl.text = "\(self.transferManager.countProgress)/\(self.transferManager.totalData)"
            let percent = Float(self.transferManager.countProgress) / Float(self.transferManager.totalData)
            self.transferProgress.setProgress(percent, animated: true)
            if error == nil && percent > 0.99 {
                self.resetProgress()
                self.transferManager.send(code: "sendSuccess")
                self.present(UIHelper.showAlert(title: "Success!", message: "success!"), animated: true, completion: nil)
                }else if error != nil{
                self.resetProgress()
                self.transferManager.send(code: "fail")
                self.present(UIHelper.showAlert(title: "Errors", message: (error?.localizedDescription)!), animated: true, completion: nil)
            }
        }
    }
    
    func resetProgress(){
        self.transferManager.totalData = 0
        self.transferManager.countProgress = 0
        self.transferProgress.setProgress(0, animated: true)
        self.countLbl.text = ""
    }
}

//contact
extension SelectDataTransferViewController: CNContactPickerDelegate {
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contacts: [CNContact]) {
        if contacts.count > 0 {
            self.urlContact = DataHelper.shareInstance.exportContactCSV(contacts: contacts)
            self.totalData += 1
        }
    }
    
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        print("Cancel Contact Picker")
    }
}


//methods get events from calendar
extension SelectDataTransferViewController {
    
    // get events from calendar
    func loadEvents() {
        
        for calendar in self.calendars! {
            if calendar.title == calendarChoosed?[0] {
                let oneMonthAgo = Date(timeIntervalSinceNow: -30*24*3600)
                let oneMonthAfter = Date(timeIntervalSinceNow: +30*24*3600)
                
                let predicate = eventStore.predicateForEvents(withStart: oneMonthAgo , end: oneMonthAfter, calendars: [calendar])
                
                self.events = eventStore.events(matching: predicate)
                
                self.totalData += 1
                break
            }
        }
    }
}

//table view calendar
extension SelectDataTransferViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let calendarCell = tableView.dequeueReusableCell(withIdentifier: "calendarCell") as? CalendarCell {
            calendarCell.updateUI(calendarTitle: (calendars?[indexPath.row].title)!)
            return calendarCell
        }
        return UITableViewCell()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (calendars?.count)!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.calendarChoosed?.append((calendars?[indexPath.row].title)!)
        self.calendarView.isHidden = true
        self.view.layer.backgroundColor = UIColor.white.cgColor
        self.transferBtn.isHidden = false
        self.loadEvents()
    }
}
