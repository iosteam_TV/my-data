//
//  TransferMacOSViewController.swift
//  mydata
//
//  Created by Nguyen Truong Dai Vi on 8/7/17.
//  Copyright © 2017 Nguyen Truong Dai Vi. All rights reserved.
//

import UIKit
import GCDWebServer

class TransferComputerViewController: UIViewController {
    
    @IBOutlet weak var filesTableView: UITableView!
    var webUpload: GCDWebUploader?
    var urlFiles = [URL]()
    static var shareWithPort = ""
    static var didShare = false
        
    override func viewDidLoad() {
        super.viewDidLoad()
        filesTableView.delegate = self
        filesTableView.dataSource = self
        self.getListFile(documentDiretory: FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!)
    }
    
    @IBAction func homeBtnPressed(_ sender: Any) {
        self.getListFile(documentDiretory: FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!)
    }
    
    @IBAction func shareBtnPressed(_ sender: Any) {
        let documentPath = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).path
        self.webUpload = GCDWebUploader(uploadDirectory: documentPath)
        webUpload?.start()
        if ((webUpload?.port) != nil) && !TransferComputerViewController.didShare {
            TransferComputerViewController.shareWithPort = "\(webUpload!.port)"
            TransferComputerViewController.didShare = true
        }
        if let addr = getWiFiAddress() {
            let link = "\(addr):\(TransferComputerViewController.shareWithPort)"
            print("link:\(addr):\(TransferComputerViewController.shareWithPort)")
            present(UIHelper.showAlert(title: "Shared", message: "Let access to: http://\(link)"), animated: true, completion: nil)
        }
    }
}


//setup tableview
extension TransferComputerViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return urlFiles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let fileCell = filesTableView.dequeueReusableCell(withIdentifier: "fileCell", for: indexPath) as? FileCell {
            let name = urlFiles[indexPath.row].lastPathComponent
            fileCell.configUI(name: name)
            return fileCell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let url = urlFiles[indexPath.row]
        if !url.lastPathComponent.contains(".") {
            self.getListFile(documentDiretory: url)
        }else if !url.path.contains("MyDataApp"){
            self.importToApp(url: urlFiles[indexPath.row])
        }else if url.path.contains("MyDataApp") {
            let alertVC = UIAlertController(title: "Message!", message: "Do you want to remove in MyDataApp?", preferredStyle: .alert)
            let cancleBtn = UIAlertAction(title: "Cancle", style: .cancel, handler: nil)
            let okBtn = UIAlertAction(title: "OK", style: .default, handler: {
                _ in
                DataHelper.shareInstance.deleteInPHAsset(imageUrls: [url])
                try! FileManager.default.removeItem(at: url)
                self.getListFile(documentDiretory: url.deletingLastPathComponent())
            })
            alertVC.addAction(cancleBtn)
            alertVC.addAction(okBtn)
            present(alertVC, animated: true, completion: nil)
        }
    }
}

//methods
extension TransferComputerViewController {
    
    func getListFile(documentDiretory: URL){
        do{
            urlFiles = try FileManager.default.contentsOfDirectory(at: documentDiretory, includingPropertiesForKeys: nil, options: [])
        }catch let err as NSError{
            print(err.debugDescription)
        }
        filesTableView.reloadData()
    }
    
    // Return IP address of WiFi interface (en2) as a String, or `nil`
    func getWiFiAddress() -> String? {
        var address : String?
        
        // Get list of all interfaces on the local machine:
        var ifaddr : UnsafeMutablePointer<ifaddrs>?
        guard getifaddrs(&ifaddr) == 0 else { return nil }
        guard let firstAddr = ifaddr else { return nil }
        
        // For each interface ...
        for ifptr in sequence(first: firstAddr, next: { $0.pointee.ifa_next }) {
            let interface = ifptr.pointee
            
            // Check for IPv4 or IPv6 interface:
            let addrFamily = interface.ifa_addr.pointee.sa_family
            if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
                
                // Check interface name:
                let name = String(cString: interface.ifa_name)
                if  name == "en0" || name == "en2" {
                    
                    // Convert interface address to a human readable string:
                    var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                    getnameinfo(interface.ifa_addr, socklen_t(interface.ifa_addr.pointee.sa_len),
                                &hostname, socklen_t(hostname.count),
                                nil, socklen_t(0), NI_NUMERICHOST)
                    address = String(cString: hostname)
                }
            }
        }
        freeifaddrs(ifaddr)
        
        return address
    }
    
    func importToApp(url: URL){
        let name = url.path
        let alertVC = UIAlertController(title: "Message!", message: "Do you want to import to MyDataApp and Library?", preferredStyle: .alert)
        let cancleBtn = UIAlertAction(title: "Cancle", style: .cancel, handler: nil)
        let okBtn = UIAlertAction(title: "OK", style: .default, handler: {
            _ in
            if name.contains("jpeg") || name.contains("png") || name.contains("mov") || name.contains("mp4") || name.contains("jpg"){
                if let data = NSData(contentsOf: url) {
                    DataHelper.shareInstance.writeImage_VideoToLibrary(data: data as Data, fileDescription: (data.description))
                    do {
                        try FileManager.default.removeItem(at: url)
                    }catch let err as NSError{
                        print(err.debugDescription)
                    }
                    self.getListFile(documentDiretory: url.deletingLastPathComponent())
                }
            }else if name.contains("contact"){
                DataHelper.shareInstance.importToContacts(url: url)
                do {
                    try FileManager.default.removeItem(at: url)
                }catch let err as NSError{
                    print(err.debugDescription)
                }
                self.getListFile(documentDiretory: url.deletingLastPathComponent())
            }else if name.contains("calendar") {
                do{
                    try DataHelper.shareInstance.importToCalendar(url: url, calendarName: "\(Date())")
                    try FileManager.default.removeItem(at: url)
                    self.getListFile(documentDiretory: url.deletingLastPathComponent())
                }catch let err as NSError{
                    print(err.debugDescription)
                }
            }
        })
        alertVC.addAction(cancleBtn)
        alertVC.addAction(okBtn)
        present(alertVC, animated: true, completion: nil)
    }
}
