//
//  UIHelper.swift
//  mydata
//
//  Created by Nguyen Truong Dai Vi on 7/20/17.
//  Copyright © 2017 Nguyen Truong Dai Vi. All rights reserved.
//

import Foundation
import UIKit

class UIHelper: UIViewController {
    
    static var shared = UIHelper()
    
    override func viewDidLoad() {

    }
    
    static func showAlert(title: String, message: String)-> UIAlertController{
        let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alertVC.addAction(alertAction)
        return alertVC
    }
}
