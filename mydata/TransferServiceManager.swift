//
//  ColorServiceManager.swift
//  ConnectedColors
//
//  Created by Ralf Ebert on 10/02/2017.
//  Copyright © 2017 Example. All rights reserved.
//

import Foundation
import MultipeerConnectivity

@objc protocol TransferServiceManagerDelegate {

    func connectedDevicesChanged(manager : TransferServiceManager, connectedDevices: [String])
    @objc optional func didReceivedCode(manager: TransferServiceManager, code: String)
    @objc optional func didReceivedData(manager: TransferServiceManager, withURL: URL?,error: Error?)
    @objc optional func didStateChangeBrowserVC(manager: TransferServiceManager)
}

class TransferServiceManager : NSObject {

    var totalData = 0
    var countProgress = 0
    // Service type must be a unique string, at most 15 characters long
    // and can contain only ASCII lowercase letters, numbers and hyphens.
    let serviceType = "transferMyData"

    let myPeerId = MCPeerID(displayName: UIDevice.current.name)

    var advertiserAssistance: MCAdvertiserAssistant?
    
    //browser v1
    var serviceBrowser : MCNearbyServiceBrowser?
    
    //browser v2 
    var browserVC: MCBrowserViewController?

    var delegate : TransferServiceManagerDelegate?
    
    static var shareInstance = TransferServiceManager()

    lazy var session : MCSession = {
        let session = MCSession(peer: self.myPeerId, securityIdentity: nil, encryptionPreference: .required)
        session.delegate = self
        return session
    }()

    override init() {
        super.init()
        
        //ardvertiser
        self.advertiserAssistance = MCAdvertiserAssistant(serviceType: self.serviceType, discoveryInfo: nil, session: self.session)
        self.advertiserAssistance?.start()
        
        //browser v1
        self.serviceBrowser = MCNearbyServiceBrowser(peer: myPeerId, serviceType: serviceType)
        self.serviceBrowser?.delegate = self
        
        //browser v2
        self.browserVC = MCBrowserViewController(serviceType: self.serviceType, session: self.session)
        self.browserVC?.delegate = self
    }
}


//browserVC
extension TransferServiceManager: MCBrowserViewControllerDelegate{
    func browserViewControllerDidFinish(_ browserViewController: MCBrowserViewController) {
        self.delegate?.didStateChangeBrowserVC!(manager: self)
    }
    
    func browserViewControllerWasCancelled(_ browserViewController: MCBrowserViewController) {
        self.delegate?.didStateChangeBrowserVC!(manager: self)
    }
}

//methods send, receive
extension TransferServiceManager {
    
    //send code
    func send(code : String) {
        if self.session.connectedPeers.count > 0 {
            do {
                //send data to peers
                try self.session.send(code.data(using: .utf8)!, toPeers: session.connectedPeers, with: .reliable)
            }
            catch let error {
                NSLog("%@", "Error for sending: \(error)")
            }
        }
    }
    
    //stop
    func stop(){
        DispatchQueue.main.async {
            self.session.disconnect()
            self.serviceBrowser?.stopBrowsingForPeers()
        }
    }
    
    //start
    func start(){
        DispatchQueue.main.async {
            self.serviceBrowser?.startBrowsingForPeers()
        }
    }

    // send file
    func sendFile(data: Data, name: String){
        let date = Date()
        let url = FileManager.default.temporaryDirectory.appendingPathComponent("\(name)_\(date)")
        //write data to url
        try? data.write(to: url)
        
        if session.connectedPeers.count > 0 {
            for peer in session.connectedPeers {
                print("send file to peer \(session.connectedPeers[0].displayName)")
                if self.session.connectedPeers.count == 0 {
                    self.delegate?.connectedDevicesChanged(manager: self, connectedDevices: [])
                    return
                }
                //send url to peer
                _ = self.session.sendResource(at: url, withName: (url.lastPathComponent), toPeer: peer, withCompletionHandler: {
                    err in
                    if err == nil {
                        print("send success!")
                    }else {
                        print("send fail!")
                    }
                })
            }
        }
    }
    
    //methods handle import to library when received data
    func receiveFile(url: URL, resourceName: String) {
        do {
            if resourceName.contains(Type.calendar) {
                try DataHelper.shareInstance.importToCalendar(url: url, calendarName: "TransferCalendar")
            }else if resourceName.contains(Type.contact) {
                DataHelper.shareInstance.importToContacts(url: url)
            }else if resourceName.contains(Type.image) || resourceName.contains(Type.video)  {
                let data = NSData(contentsOf: url)
                DataHelper.shareInstance.writeImage_VideoToLibrary(data: data! as Data, fileDescription: url.path)
            }
        }catch let err {
            print(err.localizedDescription)
        }
    }

}

// implement MCNearbyServiceBrowserDelegate
extension TransferServiceManager : MCNearbyServiceBrowserDelegate {

    func browser(_ browser: MCNearbyServiceBrowser, didNotStartBrowsingForPeers error: Error) {
        NSLog("%@", "didNotStartBrowsingForPeers: \(error)")
    }

    // invite peer
    func browser(_ browser: MCNearbyServiceBrowser, foundPeer peerID: MCPeerID, withDiscoveryInfo info: [String : String]?) {
        NSLog("%@", "foundPeer: \(peerID)")
        NSLog("%@", "invitePeer: \(peerID)")
        browser.invitePeer(peerID, to: self.session, withContext: nil, timeout: 20)
    }

    func browser(_ browser: MCNearbyServiceBrowser, lostPeer peerID: MCPeerID) {
        NSLog("%@", "lostPeer: \(peerID)")
        print("test1")
        self.delegate?.connectedDevicesChanged(manager: self, connectedDevices: [])
    }
    
}



// implement MCSessionDelegate to handle send and receive data
extension TransferServiceManager : MCSessionDelegate {

    //display peer connected
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
        NSLog("%@", "peer \(peerID) didChangeState: \(state)")
        print("Test:\(session.connectedPeers.map{$0.displayName})")
        self.delegate?.connectedDevicesChanged(manager: self, connectedDevices:
            session.connectedPeers.map{$0.displayName})
    }
    
    //handle after receive Data
    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
        let code = String(data: data, encoding: .utf8)!
        print("Data received :\(code)")
        self.delegate?.didReceivedCode!(manager: self, code: code)
    }

    func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
        NSLog("%@", "didReceiveStream")
    }

    func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) {
        NSLog("%@", "didStartReceivingResourceWithName")
    }

    //handle after receive URL from local device
    func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL, withError error: Error?) {
        if error == nil {
            
            if self.session.connectedPeers.count == 0 {
                self.delegate?.connectedDevicesChanged(manager: self, connectedDevices: [])
                return
            }
            
            var desURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            desURL.appendPathComponent(resourceName)
            
            print("==================\(desURL)")
            do {
                try FileManager.default.moveItem(atPath: localURL.path, toPath: desURL.path)
                
                //import datas to library
                DispatchQueue.main.async {
                    self.receiveFile(url: desURL, resourceName: resourceName)
                }
                self.delegate?.didReceivedData!(manager: self, withURL: desURL, error: error)
                
            } catch {
                print("[Error] \(error)")
                self.delegate?.didReceivedData!(manager: self, withURL: desURL, error: error)
            }
        }else {
            self.delegate?.didReceivedData!(manager: self, withURL: nil, error: error)
        }
        
    }

}
