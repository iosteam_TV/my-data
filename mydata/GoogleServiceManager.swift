//
//  GoogleServiceManager.swift
//  mydata
//
//  Created by Nguyen Truong Dai Vi on 7/7/17.
//  Copyright © 2017 Nguyen Truong Dai Vi. All rights reserved.
//

import Foundation
import GoogleAPIClientForREST
import GoogleSignIn
import Photos

@objc
protocol GoogleServiceDelegate {
    @objc optional
    func didSignInUser(serviceManager: GoogleServiceManager, error: Error?)
    @objc optional
    func didBackupFile(serviceManager: GoogleServiceManager, error: Error?)
    @objc optional
    func dicRestoreFile(serviceManager: GoogleServiceManager, url: URL?, error: Error?)
}

class GoogleServiceManager: NSObject {
    
    var delegate: GoogleServiceDelegate?
    var fileImgVideos = [GTLRDrive_File]()
    var fileContactCalendars = [GTLRDrive_File]()
    static var shareInstance = GoogleServiceManager()

    // If modifying these scopes, delete your previously saved credentials by
    // resetting the iOS simulator or uninstall the app.
    let scopes = [kGTLRAuthScopeDriveFile, kGTLRAuthScopeDriveAppdata]
    
    let service = GTLRDriveService()
    var folderID = ""

    
    override init() {
        super.init()
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().scopes = scopes
        GIDSignIn.sharedInstance().signInSilently()
    }
    
}

//methods service: creat folder, get fileID, backup, download

extension GoogleServiceManager {
    
    //creat folder in google drive
    func createFolder(){
        let metadata = GTLRDrive_File()
        metadata.name = "MyDataApp"
        metadata.mimeType = "application/vnd.google-apps.folder"
        let query = GTLRDriveQuery_FilesCreate.query(withObject: metadata, uploadParameters: nil)
        query.fields = "id"
        DispatchQueue.main.async {
            self.service.executeQuery(query, completionHandler: {
                ticket, file, error in
                if (error == nil) {
                    self.folderID = (file as! GTLRDrive_File).identifier!
                    print("folderID: \(String(describing: self.folderID))")
                } else {
                    print("create folder error:\(error.debugDescription)")
                    ticket.resumeUpload()
                }
            })
        }
        
    }
    
    //backup data methods
    func backupData(withFileData data: NSData, name: String, type: String){
        // login google drive silently
        GIDSignIn.sharedInstance().signInSilently()
        let fileData = data
        var metadata = GTLRDrive_File()
        
        let date = Date()
        metadata.name = "\(date)_\(name)"
        let parents: [String?] = [folderID]
        metadata.parents = parents as? [String]
        
        //setup mimeType and uploadParameter
        var uploadParameters = GTLRUploadParameters()
        if type == Type.CSV {
            
            metadata.mimeType = "application/vnd.google-apps.spreadsheet"
            uploadParameters = GTLRUploadParameters(data: (fileData as NSData) as Data, mimeType: "text/csv")

        }else if type == Type.image{
            metadata.mimeType = "image/jpeg"
            uploadParameters = GTLRUploadParameters(data: (fileData as NSData) as Data, mimeType: "image/jpeg")
        }else if type == Type.video {
            metadata.mimeType = "video/mp4"
            uploadParameters = GTLRUploadParameters(data: (fileData as NSData) as Data, mimeType: "video/mp4")
        }
        
        uploadParameters.shouldUploadWithSingleRequest = true
        
        //setup query
        let query = GTLRDriveQuery_FilesCreate.query(withObject: metadata, uploadParameters: uploadParameters)
        query.fields = "id"
        
        //execute query
        DispatchQueue.main.async {
            self.service.executeQuery(query) { (ticket, file, error) in
                if let err = error {
                    print(err.localizedDescription)
                    self.delegate?.didBackupFile!(serviceManager: self, error: error)
                    ticket.resumeUpload()
                }else {
                    self.delegate?.didBackupFile!(serviceManager: self, error: nil)
                    metadata = GTLRDrive_File()
                }
                
            }
        }
        
    }
    

    //get fileIDs
    func getFileIDs(type : String, complete: ((_ IDs:[String])->())? = nil){
        GIDSignIn.sharedInstance().signInSilently()
        
        //setup query
        let query = GTLRDriveQuery_FilesList.query()
        if type == "image/video" {
            query.q = "mimeType = 'video/mp4' or mimeType = 'image/jpeg' and '\(self.folderID)' in parents"
        }else if type == "text/csv" {
            query.q = "mimeType = 'application/vnd.google-apps.spreadsheet' and '\(self.folderID)' in parents"
        }else if type == "folder" {
            query.q = "mimeType = 'application/vnd.google-apps.folder' and name = 'MyDataApp'"
        }
        query.spaces = "drive"
        query.fields = "nextPageToken, files(id, name)"
        var IDs: [String] = []
        //execute query
        DispatchQueue.main.async {
            self.service.executeQuery(query, completionHandler: {
                ticket, files, err in
                if (err == nil) {
                    for file in (files as! GTLRDrive_FileList).files! {
                        IDs.append(file.identifier!)
                        if file.debugDescription.contains(Type.image) || file.debugDescription.contains(Type.video) {
                            self.fileImgVideos.append(file)
                        }else if file.debugDescription.contains("contacts") || file.debugDescription.contains("calendar"){
                            self.fileContactCalendars.append(file)
                        }else {
                            self.folderID = file.identifier!
                        }
                    }
                    complete!(IDs)
                } else {
                    print("error search file!")
                }
            })
            
        }
        
    }
    
    //download image, video file with fileID
    func downloadImageAndPhotoFile() {
        //login before
        GIDSignIn.sharedInstance().signInSilently()
        
        if self.fileImgVideos.count > 0 {
            var i = 0
            while i < fileImgVideos.count {
                
                //setup query
                let query: GTLRQuery = GTLRDriveQuery_FilesGet.queryForMedia(withFileId: fileImgVideos[i].identifier!)
                
                //execute query
                DispatchQueue.main.async {
                    self.service.executeQuery(query, completionHandler: {
                        ticket, file, err in
                        if err != nil {
                            print(err.debugDescription)
                            ticket.resumeUpload()
                        }else {
                            let fileData = (file as! GTLRDataObject)
                            let data = fileData.data
                            
                            //write image or video to library
                            DataHelper.shareInstance.writeImage_VideoToLibrary(data: data, fileDescription: fileData.description)
                        }
                        self.delegate?.dicRestoreFile!(serviceManager: self, url: nil, error: err)
                    })
                }
                
                i += 1
            }
            
        }
    }

    //download calendars, contacts file with fileID
    func downloadCalendarAndContactsFile(complete: @escaping (URL, Error?)->()) {
        //login before
        GIDSignIn.sharedInstance().signInSilently()
        
        if self.fileContactCalendars.count > 0 {
            var i = 0
            while i < fileContactCalendars.count {
                let date = Date()
                
                //setup url
                var filename = ""
                if (fileContactCalendars[i].name?.contains("contacts"))! {
                    filename = "\(date)_contacts_\(i).csv"
                }else if (fileContactCalendars[i].name?.contains("calendar"))! {
                    filename = "\(date)_calendar_\(i).csv"
                }
                DataHelper.shareInstance.createDirectory(name: "MyDataApp")
                let url = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent("MyDataApp/\(filename)")
//                let url = FileManager.default.temporaryDirectory.appendingPathComponent("MyDataApp/\(filename)")
                
                //setup query
                let query: GTLRQuery = GTLRDriveQuery_FilesExport.queryForMedia(withFileId: fileContactCalendars[i].identifier!, mimeType: "text/csv")
                
                //execute query
                DispatchQueue.main.async {
                    self.service.executeQuery(query, completionHandler: {
                        ticket, file, err in
                        
                        if let file = file as? GTLRDataObject{
                            let fileData = file
                            let data = fileData.data
                            //write data to url
                            try? data.write(to: url)
                        }
                        
                        complete(url,err)
                        self.delegate?.dicRestoreFile!(serviceManager: self, url: url, error: err)
                    })
                }
                i += 1
            }
        }
    }

    //check creat folder 
    func checkCreateFolder() {
        if self.isSignIn() {
            self.getFileIDs(type: "folder",complete: {
                IDs in
                if IDs.count == 0 {
                    self.createFolder()
                }
            })
        }
    }
    
    //sign in sightly
    func signInSilently() {
        GIDSignIn.sharedInstance().signInSilently()
    }
    
    // check is sign in
    func isSignIn() -> Bool {
        if GIDSignIn.sharedInstance().currentUser != nil {
            return true
        }
        return false
    }
    
    //logout
    func logout(){
        GIDSignIn.sharedInstance().signOut()
    }
}

extension GoogleServiceManager: GIDSignInDelegate, GIDSignInUIDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if (error) != nil {
            self.service.authorizer = nil
        } else {
            self.service.authorizer = user.authentication.fetcherAuthorizer()
        }
        delegate?.didSignInUser!(serviceManager: self, error: error)
    }
}



