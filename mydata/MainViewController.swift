//
//  MainViewController.swift
//  mydata
//
//  Created by Nguyen Truong Dai Vi on 7/4/17.
//  Copyright © 2017 Nguyen Truong Dai Vi. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
            UIApplication.shared.isIdleTimerDisabled = true
    }

}
