//
//  calendarCellTableViewCell.swift
//  mydata
//
//  Created by Nguyen Truong Dai Vi on 7/12/17.
//  Copyright © 2017 Nguyen Truong Dai Vi. All rights reserved.
//

import UIKit

class CalendarCell: UITableViewCell {

    @IBOutlet weak var calendarLbl: UILabel!
    func updateUI(calendarTitle: String) {
        calendarLbl.text = calendarTitle
    }

}
