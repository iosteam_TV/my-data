//
//  BackUpViewController.swift
//  mydata
//
//  Created by Nguyen Truong Dai Vi on 7/9/17.
//  Copyright © 2017 Nguyen Truong Dai Vi. All rights reserved.
//

import UIKit
import Photos
import MobileCoreServices
import DKImagePickerController // handle choose multiple image, video
import Contacts
import ContactsUI
import EventKit
import EventKitUI

class BackUpViewController: UIViewController {

    // declare pickerController to handle image, video picker
    var pickerController = DKImagePickerController()
    var listVideo: [NSData?] = []
    var listImage: [NSData?] = []
    var saveAmountImgVideo = 0
    
    //save contact to url
    var urlContact: URL?
    
    var googleServiceManager = GoogleServiceManager.shareInstance
    
    // declare to prepare handle calendar event
    var events: [EKEvent] = []
    var eventStore = EKEventStore()
    var calendars: [EKCalendar]?
    var calendarChoosed: [String]?
    
    //progress
    var countProgress = 0
    var totalData = 0
    
    
    @IBOutlet weak var backupBtn: UIButton!
    @IBOutlet weak var calendarTableView: UITableView!
    
    @IBOutlet weak var calendarView: UIView!
    @IBOutlet weak var backupProgress: UIProgressView!

    @IBOutlet weak var countLbl: UILabel!

    @IBOutlet weak var photoSwitch: UISwitch!
    @IBOutlet weak var contactSwitch: UISwitch!
    
    @IBOutlet weak var calendarSwitch: UISwitch!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //set up calendar view
        calendarView.isHidden = true
        calendarView.layer.cornerRadius = 10
        calendarView.layer.shadowColor = UIColor.black.cgColor
        calendarView.layer.shadowOffset = CGSize.zero
        calendarView.layer.shadowOpacity = 1
        
        //get permision calendar
        DataHelper.shareInstance.SOGetPermissionCalendarAccess()
        
        //initial calendarChoose
        calendarChoosed = []
        
        //setup image picker
        pickerController.showsCancelButton = true
        
        ////signin silent google drive and check already folder
        self.googleServiceManager.delegate = self
        self.googleServiceManager.signInSilently()
        self.googleServiceManager.checkCreateFolder()
        
        //setup progress
        self.totalData = 0
        self.countProgress = 0
        self.backupProgress.setProgress(0, animated: true)
    }
    
    //handel calendar switched
    @IBAction func calendarSwiched(_ sender: UISwitch) {
        if sender.isOn {
            self.calendars = eventStore.calendars(for: .event)
            self.calendarTableView.delegate = self
            self.calendarTableView.dataSource = self
            self.calendarView.isHidden = false
            self.view.layer.backgroundColor = UIColor.darkGray.cgColor
            self.backupBtn.isHidden = true
            self.calendarTableView.reloadData()
        }else {
            if totalData > 0 && (events.count) > 0 {
                self.totalData -= 1
            }
            calendarChoosed = []
            events = []
        }
    }
    
    //handel contact switched
    @IBAction func contactSwitched(_ sender: UISwitch) {
        if sender.isOn {
            let cnPicker = CNContactPickerViewController()
            cnPicker.delegate = self
            self.present(cnPicker, animated: true, completion: nil)
        } else {
            print(totalData)
            if totalData > 0 && urlContact != nil{
                self.totalData -= 1
            }
            urlContact = nil
        }
    }
    
    //handel photo, video switched
    @IBAction func backupPhotoSwitched(_ sender: UISwitch) {
        if sender.isOn {
            self.present(pickerController, animated: true) {}
            pickerController.didSelectAssets = { (assets: [DKAsset]) in
                self.totalData += assets.count
                self.saveAmountImgVideo = assets.count
                for i in 0..<assets.count {
                    if assets[i].isVideo {
                        DataHelper.shareInstance.getDataAV(asset: assets[i], name: "\(i)", complete: {
                            data in
                            self.listVideo.append(data!)
                        })
                    } else {
                        assets[i].fetchImageDataForAsset(true, completeBlock: {
                            data, info in
                            self.listImage.append(data! as NSData)
                        })
                    }
                }
            }
            print("didSelectAssets")
        } else {
            if totalData > 0 {
                self.totalData -= saveAmountImgVideo
                saveAmountImgVideo = 0
            }
            
            listVideo = []
            listImage = []
        }
    }
    
    @IBAction func cancelBtnPressed(_ sender: Any) {
        calendarView.isHidden = true
        self.view.layer.backgroundColor = UIColor.white.cgColor
        self.backupBtn.isHidden = false
    }
    
    //handel backup pressed
    @IBAction func backupBtnPressed(_ sender: Any) {
        if listImage.count > 0 || listVideo.count > 0 || urlContact != nil || (events.count) > 0 {
            present(UIHelper.showAlert(title: "Backuping", message: "please wait..."), animated: true, completion: nil)
        }
        //send image
        if listImage.count > 0 {
            var i = 1
            for data in listImage {
                googleServiceManager.backupData(withFileData: data!, name: "\(Type.image)\(i)", type: Type.image)
                i += 1
            }
            listImage = []
        }
        
        // send video
        if listVideo.count > 0 {
            var i = 1
            for data in listVideo {
                googleServiceManager.backupData(withFileData: data!, name: "\(Type.video)\(i)", type: Type.video)
                i += 1
            }
            listVideo = []
        }
        
        // send contacts
        if self.urlContact != nil {
            let data = try? Data(contentsOf: urlContact!)
            googleServiceManager.backupData(withFileData: data! as NSData, name: "contacts", type: Type.CSV)
            urlContact = nil
        }
        
        //send calendar 
        if (self.calendarChoosed?.count)! > 0 {
            let url = DataHelper.shareInstance.writeEventCalendarToTextFile(events: self.events, name: "Backup")
            let data = NSData(contentsOf: url!)
            googleServiceManager.backupData(withFileData: data!, name: "calendar", type: Type.CSV)
            calendarChoosed = []
        }
        //reset all
        self.pickerController.deselectAllAssets()
        self.photoSwitch.isOn = false
        self.contactSwitch.isOn = false
        self.calendarSwitch.isOn = false
        saveAmountImgVideo = 0
    }
    
    
    @IBAction func logoutBtnPressed(_ sender: UIButton) {
        googleServiceManager.logout()
        navigationController?.popViewController(animated: true)
    }
    
}

//implement methods of GoogleServiceDelegate
extension BackUpViewController: GoogleServiceDelegate {
    
    func didSignInUser(serviceManager: GoogleServiceManager, error: Error?) {
        if error != nil {
            present(UIHelper.showAlert(title: "Errors", message: "You have not account yet! Let's Signin."), animated: true, completion: nil)
        }else {
            print("did sign in!")
        }
    }
    
    func didBackupFile(serviceManager: GoogleServiceManager, error: Error?) {
        DispatchQueue.main.async {
            self.countProgress += 1
            self.countLbl.text = "\(self.countProgress)/\(self.totalData)"
            let percent = Float(self.countProgress) / Float(self.totalData)
            self.backupProgress.setProgress(percent, animated: true)
            if error == nil && percent > 0.95  {
                self.countProgress = 0
                self.totalData = 0
                self.backupProgress.setProgress(0, animated: true)
                self.countLbl.text = ""
                self.dismiss(animated: false, completion: nil)
                self.present(UIHelper.showAlert(title: "Success",message: "Backup success!"), animated: true, completion: nil)
            }else if error != nil{
                self.dismiss(animated: false, completion: nil)
                self.present(UIHelper.showAlert(title: "Error",message: (error?.localizedDescription)!), animated: true, completion: nil)
            }
        }
    }
    
    func dicRestoreFile() {
        print("did restore!")
    }
}


extension BackUpViewController: CNContactPickerDelegate {
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contacts: [CNContact]) {
        if contacts.count > 0 {
            self.urlContact = DataHelper.shareInstance.exportContactCSV(contacts: contacts)
            self.totalData += 1
        }
        print(self.totalData)
    }
    
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        print("Cancel Contact Picker")
    }
}


//methods get events from calendar
extension BackUpViewController {
    
    // get events from calendar
    func loadEvents() {
        
        for calendar in self.calendars! {
            if calendar.title == calendarChoosed?[0] {
                let oneMonthAgo = Date(timeIntervalSinceNow: -30*24*3600)
                let oneMonthAfter = Date(timeIntervalSinceNow: +30*24*3600)
                
                let predicate = eventStore.predicateForEvents(withStart: oneMonthAgo , end: oneMonthAfter, calendars: [calendar])
                
                self.events = eventStore.events(matching: predicate)
                self.totalData += 1
                break
            }
        }
    }
}

//methods implement UITableViewDelegate, UITableViewDataSource
extension BackUpViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let calendarCell = tableView.dequeueReusableCell(withIdentifier: "calendarCell") as? CalendarCell {
            calendarCell.updateUI(calendarTitle: (calendars?[indexPath.row].title)!)
            return calendarCell
        }
        return UITableViewCell()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (calendars?.count)!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.calendarChoosed?.append((calendars?[indexPath.row].title)!)
        self.calendarView.isHidden = true
        self.view.layer.backgroundColor = UIColor.white.cgColor
        self.backupBtn.isHidden = false
        self.loadEvents()
    }
}
