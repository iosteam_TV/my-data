//
//  RestoreDataViewController.swift
//  mydata
//
//  Created by Nguyen Truong Dai Vi on 7/5/17.
//  Copyright © 2017 Nguyen Truong Dai Vi. All rights reserved.
//

import UIKit
import Contacts
import EventKit

class RestoreDataViewController: UIViewController {
    
    var googleServiceManager = GoogleServiceManager.shareInstance
    var checkRestoreImgVid = false
    var checkRestoreContactCal = false
    let eventStore : EKEventStore = EKEventStore()
    var countProgress = 0
    var totalData = 0
    var saveAmountImgVideo = 0
    var saveAmountContactCalendar = 0
    
    @IBOutlet weak var restoreProgress: UIProgressView!
    
    @IBOutlet weak var countLbl: UILabel!
    @IBOutlet weak var photoSwitch: UISwitch!
    @IBOutlet weak var contactCalendarSwitch: UISwitch!
    override func viewDidLoad() {
        //signin silent google drive and check already folder
        googleServiceManager.delegate = self
        googleServiceManager.signInSilently()
        googleServiceManager.checkCreateFolder()
        
        //set up progress
        totalData = 0
        countProgress = 0
        restoreProgress.setProgress(0, animated: true)
    }
    
    
    @IBAction func photoVideoSwitched(_ sender: UISwitch) {
        if sender.isOn {
            googleServiceManager.getFileIDs(type: "image/video", complete: {
                ids in
                self.totalData += ids.count
                self.saveAmountImgVideo = ids.count
                print(self.saveAmountImgVideo)
            })
            checkRestoreImgVid = true
        }else {
            if totalData > 0 && saveAmountImgVideo > 0{
                self.totalData -= self.saveAmountImgVideo
                self.saveAmountImgVideo = 0
                checkRestoreImgVid = false
            }
        }
    }
    
    @IBAction func contactCalendarSwitched(_ sender: UISwitch) {
        if sender.isOn {
            googleServiceManager.getFileIDs(type: "text/csv", complete: {
                ids in
                self.totalData += ids.count
                self.saveAmountContactCalendar = ids.count
                print("file: \(self.saveAmountContactCalendar)")
            })
            checkRestoreContactCal = true
        }else {
            if totalData > 0 && saveAmountContactCalendar > 0{
                self.totalData -= self.saveAmountContactCalendar
                self.saveAmountContactCalendar = 0
                checkRestoreContactCal = false
            }
        }
    }
    
    @IBAction func restoreBtnPressed(_ sender: UIButton) {
        
        present(UIHelper.showAlert(title: "Restoring", message: "Please wait..."), animated: true, completion: nil)
        
        //restore image, video
        if checkRestoreImgVid {
            googleServiceManager.downloadImageAndPhotoFile()
        }
        //restore contact, calendar
        if checkRestoreContactCal {
            googleServiceManager.downloadCalendarAndContactsFile(complete: { (url, err) in
                if url.path.contains("contacts") {
                    DataHelper.shareInstance.importToContacts(url: url)
                }
                if url.path.contains("calendar") {
                    do {
                        try DataHelper.shareInstance.importToCalendar(url: url, calendarName: "Restore Calendar")
                    }catch let error {
                        self.present(UIHelper.showAlert(title: "Errors", message: error.localizedDescription), animated: true, completion: nil)
                    }
                }
            })
        }
        self.contactCalendarSwitch.isOn = false
        self.photoSwitch.isOn = false
        saveAmountImgVideo = 0
        saveAmountContactCalendar = 0
    }
    
    
    @IBAction func logoutBtnPressed(_ sender: UIButton) {
        googleServiceManager.logout()
        navigationController?.popViewController(animated: true)
    }
    
}

// implement methods of GoogleServiceDelegate
extension RestoreDataViewController: GoogleServiceDelegate {
    func didSignInUser(serviceManager: GoogleServiceManager, error: Error?) {
        if error != nil {
            print("sign success!")
        }
    }
    
    func dicRestoreFile(serviceManager: GoogleServiceManager, url: URL?, error: Error?) {
        DispatchQueue.main.async {
            self.countProgress += 1
            self.countLbl.text = "\(self.countProgress)/\(self.totalData)"
            let percent = Float(self.countProgress) / Float(self.totalData)
            self.restoreProgress.setProgress(percent, animated: true)
            print("count: \(String(describing: self.countProgress))")
            print("total: \(String(describing: self.totalData))")
            print("percent: \(percent)")

            if error == nil && percent > 0.95 {
                self.totalData = 0
                self.countProgress = 0
                self.restoreProgress.setProgress(0, animated: true)
                self.countLbl.text = ""
                self.dismiss(animated: false, completion: nil)
                self.present(UIHelper.showAlert(title: "Success", message: "Restore success!"), animated: true, completion: nil)
            }else if error != nil{
                self.dismiss(animated: false, completion: nil)
                self.present(UIHelper.showAlert(title: "Errors", message: (error?.localizedDescription)!), animated: true, completion: nil)
            }
        }
    }
}
