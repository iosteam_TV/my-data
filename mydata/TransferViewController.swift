//
//  TestViewController.swift
//  mydata
//
//  Created by Nguyen Truong Dai Vi on 7/4/17.
//  Copyright © 2017 Nguyen Truong Dai Vi. All rights reserved.
//

import UIKit

class TransferViewController: UIViewController{

    @IBOutlet weak var connectionsLabel: UILabel!
    
    @IBOutlet weak var disconnectBtn: UIButton!
    
    @IBOutlet weak var loadBtn: UIButton!
    @IBOutlet weak var countLbl: UILabel!
    
    let transferService = TransferServiceManager.shareInstance
    var checkCode: Bool = false
    var checkNext: Bool =  false
    static var connectedPeer = ""
    var totalData = 0
    var countProgess = 0
    var blueColor = UIColor(red:0.28, green:0.54, blue:0.97, alpha:1.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //reset
        self.reset()
        self.resetProgress()
        transferService.stop()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        transferService.delegate = self
        if transferService.session.connectedPeers.count == 0 {
            TransferViewController.connectedPeer = "No connection!"
            self.reset()
            self.resetProgress()
            transferService.stop()
        }
        self.connectionsLabel.text = "Connections: \(TransferViewController.connectedPeer)"
        if self.transferService.totalData > 0{
            self.countLbl.text = "Received:\(self.transferService.countProgress)/\(self.transferService.totalData)"
        }else {
            self.resetProgress()
        }
    }
    
    @IBAction func loadBtnPressed(_ sender: UIButton) {
        self.present(self.transferService.browserVC!, animated: true, completion: nil)
    }
    
    @IBAction func disconnectBtnPressed(_ sender: UIButton) {
        self.transferService.stop()
        self.reset()
        self.resetProgress()
    } 
    
    @IBAction func nextBtnPressed(_ sender: UIButton) {
        if self.transferService.session.connectedPeers.count > 0 {
            transferService.send(code: "next")
            performSegue(withIdentifier: "selectData", sender: nil)
        }
    }
    
    @IBAction func tapHandle(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
}

//browserVC
extension TransferViewController {
    func didStateChangeBrowserVC(manager: TransferServiceManager) {
        self.dismiss(animated: true, completion: nil)
    }
}

//methods util
extension TransferViewController {
    
    func reset(){
        //setup view
        self.connectionsLabel.text = "No connection!"
        TransferViewController.connectedPeer = "No connection!"
        self.disconnectBtn.isEnabled = false
        self.disconnectBtn.setTitleColor(UIColor.gray, for: .normal)
    }
}

extension TransferViewController : TransferServiceManagerDelegate {
    
    func connectedDevicesChanged(manager: TransferServiceManager, connectedDevices: [String]) {
        DispatchQueue.main.async {
            if connectedDevices.count > 0 {
                TransferViewController.connectedPeer = "\(connectedDevices)"
                print("connected!")
                //setup view
                self.disconnectBtn.isEnabled = true
                self.disconnectBtn.setTitleColor(self.blueColor, for: .normal)
                self.dismiss(animated: true, completion: nil)
            }else {
                print("no connected!")
                TransferViewController.connectedPeer = "No Connections!"
                self.reset()
            }
            self.connectionsLabel.text = "Connections: \(TransferViewController.connectedPeer)"
        }
    }

    func didReceivedCode(manager: TransferServiceManager, code: String) {
        DispatchQueue.main.async {
            if code == "next" {
                self.performSegue(withIdentifier: "selectData", sender: nil)
            }else if code.contains("total") {
                let values = code.components(separatedBy: ":")
                self.totalData = Int(values[1])!
            }
        }
    }
    
    func didReceivedData(manager: TransferServiceManager, withURL: URL?, error: Error?) {
        DispatchQueue.main.async {
            self.transferService.countProgress += 1
            self.countLbl.text = "Received:\(self.transferService.countProgress)/\(self.transferService.totalData)"
            print("total: \(self.transferService.totalData) count: \(self.transferService.countProgress)")
            if error == nil && (self.transferService.countProgress - self.transferService.totalData) == 0 {
                self.resetProgress()
                self.transferService.send(code: "sendSuccess")
                self.present(UIHelper.showAlert(title: "Success!", message: "success!"), animated: true, completion: nil)
            }else if error != nil{
                self.resetProgress()
                self.transferService.send(code: "fail")
                self.present(UIHelper.showAlert(title: "Errors", message: (error?.localizedDescription)!), animated: true, completion: nil)
            }
        }
    }
    
    func resetProgress(){
        self.transferService.totalData = 0
        self.transferService.countProgress = 0
        self.countLbl.text = ""
    }
    
}
