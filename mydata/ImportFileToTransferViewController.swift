//
//  ImportFileToTransferViewController.swift
//  mydata
//
//  Created by Nguyen Truong Dai Vi on 8/11/17.
//  Copyright © 2017 Nguyen Truong Dai Vi. All rights reserved.
//

import UIKit
import Photos
import MobileCoreServices
import DKImagePickerController // handle choose multiple image, video
import Contacts
import ContactsUI
import EventKit
import EventKitUI

class ImportFileToTransferViewController: UIViewController {

    var pickerController = DKImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //get permision calendar
        DataHelper.shareInstance.SOGetPermissionCalendarAccess()
        
        DataHelper.shareInstance.createDirectory(name: "FileToShare")
    }
    
    @IBAction func photoBtnPressed(_ sender: UIButton) {
        DataHelper.shareInstance.createDirectory(name: "FileToShare")
        self.present(pickerController, animated: true) {}
        pickerController.didSelectAssets = { (assets: [DKAsset]) in
            for i in 0..<assets.count {
                if assets[i].isVideo {
                    DataHelper.shareInstance.getDataAV(asset: assets[i], name: "\(i)", complete: { data in
                        let name = "vid_\(Date())_\(i).mov"
                        do{
                            let url = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent("FileToShare/\(name)")
                            data?.write(to: url, atomically: true)
                        }catch let err as NSError {print(err.debugDescription)}
                    })
                } else {
                    assets[i].fetchImageDataForAsset(true, completeBlock: {
                        data, info in
                        let name = "img_\(Date())_\(i).jpeg"
                        let url = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent("FileToShare/\(name)")
                        do{
                            try data?.write(to: url)
                        }catch let err as NSError{print(err.debugDescription)}
                    })
                }
            }
        }
        print("didSelectAssets")
    }
    
    @IBAction func contactBtnPressed(_ sender: UIButton) {
        DataHelper.shareInstance.createDirectory(name: "FileToShare")
        let cnPicker = CNContactPickerViewController()
        cnPicker.delegate = self
        self.present(cnPicker, animated: true, completion: nil)
    }
    
    
    @IBAction func calendarBtnPressed(_ sender: UIButton) {
        DataHelper.shareInstance.createDirectory(name: "FileToShare")
        let alertVC = UIAlertController(title: "Message", message: "Do you want to load Calendars to MyDataApp?", preferredStyle: .alert)
        let cancleBtn = UIAlertAction(title: "Cancle", style: .cancel, handler: nil)
        let okBtn = UIAlertAction(title: "OK", style: .default, handler: {
            _ in
            self.loadEvents()
        })
        alertVC.addAction(cancleBtn)
        alertVC.addAction(okBtn)
        present(alertVC, animated: true, completion: nil)
    }
}

extension ImportFileToTransferViewController: CNContactPickerDelegate {
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contacts: [CNContact]) {
        if contacts.count > 0 {
            let url = DataHelper.shareInstance.exportContactCSV(contacts: contacts)
            do{
                let shareFolder = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent("FileToShare")
                DataHelper.shareInstance.moveFile(from: url, to: shareFolder)
            }catch let err as NSError{
                print(err.debugDescription)
            }
        }
    }
    
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        print("Cancel Contact Picker")
    }
}

//methods get events from calendar
extension ImportFileToTransferViewController {
    
    // get events from calendar
    func loadEvents() {
        let eventStore = EKEventStore()
        let calendars = eventStore.calendars(for: .event)
        var index = 0
        for calendar in calendars {
            let oneMonthAgo = Date(timeIntervalSinceNow: -30*24*3600)
            let oneMonthAfter = Date(timeIntervalSinceNow: +30*24*3600)
            
            let predicate = eventStore.predicateForEvents(withStart: oneMonthAgo , end: oneMonthAfter, calendars: [calendar])
            
            let events = eventStore.events(matching: predicate)
            if events.count > 0 {
                let url = DataHelper.shareInstance.writeEventCalendarToTextFile(events: events, name: "\(calendar.title)\(index)")
                do{
                    let shareFolder = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent("FileToShare")
                    //move to Folder FileToshare
                    DataHelper.shareInstance.moveFile(from: url!, to: shareFolder)
                }catch let err as NSError{
                    print(err.debugDescription)
                }
                index += 1
            }
        }
    }
}
